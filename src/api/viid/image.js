import request from '@/utils/request'

// 查询人脸照片列表
export function listImage(query) {
  return request({
    url: '/viid/image/list',
    method: 'get',
    params: query
  })
}

// 查询人脸照片详细
export function getImage(id) {
  return request({
    url: '/viid/image/' + id,
    method: 'get'
  })
}

// 新增人脸照片
export function addImage(data) {
  return request({
    url: '/viid/image',
    method: 'post',
    data: data
  })
}

// 修改人脸照片
export function updateImage(data) {
  return request({
    url: '/viid/image',
    method: 'put',
    data: data
  })
}

// 删除人脸照片
export function delImage(id) {
  return request({
    url: '/viid/image/' + id,
    method: 'delete'
  })
}
